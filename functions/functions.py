"""
Functions for loading and filtering variant data
"""

import pickle
import sys
from typing import Union
import numpy as np
import pandas as pd
import scipy.stats as stats

def prep_variant_dataframe(tsv_file: str, nrows_to_load: int=None) -> pd.DataFrame:
    """
    Read the dataframe and perform initial cleaning of data

    tsv_file = path to variant TSV file (from previous processiung steps)
    nrows_to_load = for testing - number of variant rows to load
    """

    # Define converter functions
    def float_converter(var: str) -> np.float:
        """Converts a string to a float (if possible, returning NaN if not)"""
        try:
            return np.float(var)
        except ValueError:
            return np.nan

    def gt_converter(genotype: str) -> Union[int, np.float]:
        """Convert a genotpye (1/0, 1/1, etc.) to a numerical allele count"""
        if genotype in ["0/0"]:
            return 0
        if genotype in ["./."]:
            return -1
        if genotype in ["0/1", "1/0", "./1", "1/."]:
            return 1
        if genotype in ["1/1"]:
            return 2
        # Couldn't identify genotype - return NaN
        return np.nan

    # Specify the columns to import, rather than importing and then dropping
    columns = [
        "CHROM",
        "POS",
        "ID",
        "GENE",
        "IMPACT",
        "HGVSc",
        "HGVSp",
        "SIFT",
        "Polyphen",
        "CADD_PHRED",
        "REVEL",
        "GnomAD_AF",
        "MAT_ID",
        "MAT_GT",
        "PAT_ID",
        "PAT_GT",
        "PRO_ID",
        "PRO_GT",
    ]

    # Specify dtypes for each column, so we don't have to convert them later
    dtypes = {
        "CHROM": "category",
        "POS": "int",
        "ID": "object",
        "GENE": "category",
        "IMPACT": "category",
        "HGVSc": "object",
        "HGVSp": "object",
        "SIFT": "object",
        "Polyphen": "object",
        "MAT_ID": "object",
        "PAT_ID": "object",
        "PRO_ID": "object",
    }

    # Set converters to work on data while importing
    converters = {
        "GnomAD_AF": float_converter,
        "CADD_PHRED": float_converter,
        "REVEL": float_converter,
        "MAT_GT": gt_converter,
        "PAT_GT": gt_converter,
        "PRO_GT": gt_converter,
    }

    # DEV: Use nrows to only read the first 100,000 variants to make it more
    # practical to work on for development. Actual total variant count
    # is over 37 million
    dataframe = pd.read_csv(
        tsv_file,
        sep="\t",
        header=0,
        usecols=columns,
        dtype=dtypes,
        converters=converters,
        nrows=nrows_to_load,
    )

    print(f"Total number of variants: {len(dataframe.ID)}", file=sys.stderr)

    # Calculate the exact cohort size, excluding duplicates
    cohort_size = dataframe["MAT_ID"].unique().shape[0] # pylint: disable=E1136  # pylint/issues/3139
    cohort_chroms = cohort_size * 2

    # Add the cohort size to the dataframe
    dataframe["cohortsize"]= cohort_chroms
    #dataframe["patcohort"] = dataframe.PAT_ID.unique().shape[0] * 2
    dataframe["patcohort"] = dataframe["PAT_ID"].unique().shape[0] * 2 # pylint: disable=E1136  # pylint/issues/3139

    print(f"Overall size of cohort: {cohort_size} mothers", file=sys.stderr)
    print(f"Total chromosomes: {cohort_chroms}", file=sys.stderr)

    return dataframe


def prep_phenotype_dataframe(csv_file:str, nrows_to_load: int=None, columns: list=None) -> pd.DataFrame:
    """

    Load the phenotype data for all samples

    """

    # Define converter functions
    def pregnancy_loss_history_converter(plh: str) -> str:
        """Make the pregnancy loss values consistent"""
        if plh in ["3 or More", "3 or more"]:
            return "3+"
        return plh
    
    # Specify dtypes for each column, so we don't have to convert them later
    dtypes = {
        "consanguinity": "category",
        "assisted_reproduction": "category",
        "mothers_age": "float"
    }

    # Set converters to work on data while importing
    converters = {
        "pregnancy_loss_history": pregnancy_loss_history_converter,
    }

    # We have to drop these columns with identifiable info (of the clinician)
    columns_to_ignore = ["referring_clinician_name", "referring_clinician_email"]
    
    dataframe = pd.read_csv(csv_file,
                            header=0,
                            encoding = "ISO-8859-1",
                            usecols=columns,
                            dtype=dtypes,
                            converters=converters,
                            na_values=['Unknown'],
                            nrows=nrows_to_load
                           )

    try:                       
        dataframe = dataframe.drop(columns_to_ignore, axis='columns')
    except KeyError:
        pass

    # Cast pregnancy_loss_history column as a categorical
    dataframe.pregnancy_loss_history = dataframe.pregnancy_loss_history.astype("category")
    
    # Remove singleton samples (i.e no parental IDs)
    dataframe = dataframe[dataframe.mother_stable_id != "-"]

    return dataframe


def gather_stats(dataframe: pd.DataFrame, step: str) -> pd.DataFrame:
    """
    Calculate various bits of information about the given dataframe,
    so we don't end up with it split all over the place like it currently is.

    Should be appended to the existing dataframe so that each additional
    run adds a new column. Might need a name column, so we can keep track.
    e.g. Update after doing a filter
    """
    dataframeitems = {
        "index": step,
        "name": step,
        "varcount": None,
        "varsperchrom": None,
        "impacts": None,
    }

    # NOTE: values have to be contained in a list
    # to be read into the DataFrame as the same type
    dataframeitems["varcount"] = [len(dataframe.CHROM)]
    dataframeitems["varsperchrom"] = [dataframe.CHROM.value_counts().sort_index()]
    dataframeitems["HIGH"] = [dataframe.IMPACT.value_counts()["HIGH"]]
    for impact in dataframe.IMPACT.value_counts().index.to_list():
        dataframeitems[impact] = [dataframe.IMPACT.value_counts()[impact]]

    return pd.DataFrame(dataframeitems).set_index("index")


def calculate_frequencies(dataframe: pd.DataFrame, processed_with_zscore_pickle: str, var_frequencies_pickle: str) -> pd.DataFrame:
    """Work out the variant cohort frequencies for the vairants"""
    try:
        dataframe = pickle.load(open(processed_with_zscore_pickle, 'rb'))
        variants = pickle.load(open(var_frequencies_pickle, 'rb'))
    except FileNotFoundError:
        # Get a unique list of variants
        variants = (
            dataframe[["ID", "GENE", "GnomAD_AF", "IMPACT", "cohortsize"]]
            .drop_duplicates()
            .sort_values("ID")
        )

        # Add the number of times each variant occurs in the main dataset
        variants = variants.join(dataframe.groupby("ID")["MAT_GT"].sum(),
                                 on="ID", rsuffix="_")
        variants = variants.join(dataframe.ID.value_counts(),
                                 on="ID", rsuffix="_")
        variants = variants.rename(columns={"ID_": "COHORT_AC"})

        # Calculate the allele frequency from this allele
        # count and the cohort size.
        # DEV: This needs to account for zygosity and chrom count
        variants["COHORT_AF"] = variants["COHORT_AC"] / variants["cohortsize"]

        variants["AF_DIFF"] = variants.COHORT_AF - variants.GnomAD_AF

        # Add a Z-score column to the variant data
        # Z-score is the number of stdevs away from the
        # expected GnomAD allele frequency
        stdev = variants.AF_DIFF.std()
        variants["Z_score"] = variants.AF_DIFF / stdev

        # Add the Z-score value to the main datafram
        # create a merge-able dataframe with the Z-score
        dataframe = pd.merge(dataframe, variants[["ID", "Z_score"]], on="ID")

        variants.to_pickle(var_frequencies_pickle)
        dataframe.to_pickle(processed_with_zscore_pickle)

    return variants

def split_sift_polyphen_class(value):
    """
    Split out the class from the SIFT and Polyphen columns
    which are formatted like "Deleterious(1.000)"
    
    For indels, where there is no class, it will raise an
    exception. If this happens, just assume and return "Deleterious"
    
    Use remap_sift_polyphen_classes) to assign equivalent values to both,
    as by default they use slightly different classes.
    
    If the dataframe has already been processed, then it will not 
    be possible to split and should raise an exception. If that happens,
    check if the values match the processed values and return if so.
    """
    try:
        return remap_sift_polyphen_classes(value.split("(")[0])
    except:
        if value in ['Benign', 'Deleterious']:
            return value
        return "Deleterious"

def remap_sift_polyphen_classes(value):
    """
    Look up the possible SIFT and Polyphen classes, and return a matched
    equivalent value. e.g. both 'deleterious' and 'probably_damaging' will
    return 'Deleterious'
    """
    # SIFT classes:
    # 'deleterious_low_confidence', 'deleterious', 'tolerated', 'tolerated_low_confidence'
    
    # Polyphen classes:
    # 'benign', 'possibly_damaging', 'probably_damaging', 'Deleterious', 'unknown'
    
    classes = {'deleterious_low_confidence': 'Deleterious', 'deleterious': 'Deleterious',
               'tolerated': 'Benign', 'tolerated_low_confidence': 'Benign',
               'benign': 'Benign', 'possibly_damaging': 'Benign', 'probably_damaging': 'Deleterious',
               'Deleterious': 'Deleterious', 'unknown': 'Benign'}
    
    return classes[value]

def count_samples_with_variants(target_gene, dataframe):
    """Count how many samples in the given dataframe have at least 1 variant in the specified gene
    
    This can be modified to only count samples with variants matching particular modes of inheritence
    e.g. We can only count 
    """
    # subset the target dataframe to get only this gene
    temp_df = dataframe[dataframe.GENE == target_gene]
    # set MAT_COUNT == 2 and final == 1 to capture exclusively homozygous variants
    # set the final >= 1 to 2 (with MAT_COUNT >=1) to require at least a compound het, including hom
    # MAT_COUNT >=1 and .count == 1 should get single dominant variants
    # Currently, we are counting one or more variants
    temp_df_2 = temp_df[temp_df.MAT_GT >= 1].groupby("MAT_ID")[['MAT_GT']].count() >= 1
    
    # Return the number of samples in which the above criteria is matched
    answer = len(temp_df_2[temp_df_2.MAT_GT == True])
    
    # clear temp dataframes
    del(temp_df)
    del(temp_df_2)
    
    return answer


def do_burden_test(row):
    """Do a Fisher exact test on the case/control positive/negative counts"""
    cases = [row.loc['pos_cases'], row.loc['neg_cases']]
    controls = [row.loc['pos_controls'], row.loc['neg_controls']]
    # Use Scipy.stats fisher exact test
    # Order doesn't seem to matter as long as they are grouped correctly
    oddsratio, pvalue = stats.fisher_exact([cases, controls])
    
    if oddsratio < 1:
        # This will stop "protective" genes from appearing
        # They might be interesting, but they aren't relevant for now
        return 1
    return pvalue